 <?php
 // Based on https://gitlab.wikimedia.org/toolforge-repos/db-names
 /**
 * BSD 3-Clause License
 * Copyright (c) 2021-2023 Taavi Väänänen <hi@taavi.wtf>
 * Copyright (c) 2023-2023 David Caro <me@dcaro.es>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
?>

<?php
// This two lines avoid displaying errors in the page
error_reporting( -1 );
ini_set( 'display_errors', 1 );


// This will be how we get a SQL connection
/**
 * @return mysqli
 */
function getSqlConnection() {
    $connection = new mysqli(
        // This is the database server we want to connect to
        // we are using meta to access the meta_p database
        // See https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database for others
        'meta.web.db.svc.wikimedia.cloud',
        getenv( 'TOOL_REPLICA_USER' ),
        getenv( 'TOOL_REPLICA_PASSWORD' ),
        'meta_p'
    );

    if ($connection->connect_error) {
        throw new RuntimeException("Connection failed: " . $connection->connect_error);
    }

    return $connection;
}
?>

<?php
function getDbNames() {
    $connection = getSqlConnection();
    $statement = $connection->prepare('select dbname from wiki;');
    $statement->execute();

    if ($statement->error) {
        throw new RuntimeException("Failed to retrieve data: $statement->error");
    }

    $databases = [];

    $result = $statement->get_result();
    while ($row = $result->fetch_assoc()) {
        $databases[] = $row;
    }

    $result->close();
    $statement->close();
    $connection->close();

    return $databases;
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Database names</title>
</head>
<body>

<div class="container mt-3">
    <p>
        Sample PHP application accessing the
        <a href="https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database">wiki replicas</a>
        special <code>meta_p</code> database.
    </p>
    <div class="overflow-scroll">
        <table class="table table-hover table-sm" id="db-table">
            <thead>
                <tr>
                    <th scope="col">Database name</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach(getDbNames() as $row): ?>
                <tr>
                    <td><?= $row['dbname'] ?></td>
                </tr>
                <?php endforeach; ?>
           </tbody>
        </table>
    </div>
</div>
</body>
</html>

